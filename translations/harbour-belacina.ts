<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>AccountsPage</name>
    <message>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AddAccountPage</name>
    <message>
        <source>Add account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>URL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Username. Don&apos;t use `:` in username.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy token here</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoardPage</name>
    <message>
        <source>Change board</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BoardsPage</name>
    <message>
        <source>Boards</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Accounts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add local board</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CardPage</name>
    <message>
        <source>On %L1, %2 wrote:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Put your note here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ChangePage</name>
    <message>
        <source>Change card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change board</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New board</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ListPage</name>
    <message>
        <source>Add card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Move to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort by name descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort by name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort by time of creation descending</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sort by time of creation</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MoveToPage</name>
    <message>
        <source>Move to</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
