# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-belacina

CONFIG += sailfishapp

SOURCES += src/harbour-belacina.cpp

DISTFILES += qml/harbour-belacina.qml \
    qml/cover/CoverPage.qml \
    rpm/harbour-belacina.changes.in \
    rpm/harbour-belacina.changes.run.in \
    rpm/harbour-belacina.spec \
    rpm/harbour-belacina.yaml \
    translations/*.ts \
    harbour-belacina.desktop \
    qml/db.js \
    qml/pages/BoardsPage.qml \
    qml/pages/BoardPage.qml \
    qml/pages/ListPage.qml \
    qml/pages/CardPage.qml \
    qml/pages/ChangePage.qml \
    qml/pages/MoveToPage.qml \
    qml/pages/AddAccountPage.qml \
    qml/board.js \
    qml/pages/AccountsPage.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

# to disable building translations every time, comment out the
# following CONFIG line
CONFIG += sailfishapp_i18n

# German translation is enabled as an example. If you aren't
# planning to localize your app, remember to comment out the
# following TRANSLATIONS line. And also do not forget to
# modify the localized app name in the the .desktop file.
TRANSLATIONS += translations/harbour-belacina-de.ts
