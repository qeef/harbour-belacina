/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
.pragma library
.import QtQuick.LocalStorage 2.0 as Ls

function get_db()
{
    try {
        var db = Ls.LocalStorage.openDatabaseSync(
                    "belacina.db",
                    "",
                    "Storage for boards, lists, cards, and notes.",
                    10000000);
    } catch (err) {
        console.log("Error opening database: " + err);
    }
    return db;
}

function send_ajax(cb, method, path, cred, body)
{
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        if (xhr.readyState === XMLHttpRequest.DONE) {
            if (xhr.status === 200) {
                cb(JSON.parse(xhr.responseText));
            } else {
                console.log("NextCloud connection failed!");
                console.log(xhr.responseText);
                cb([]);
            }
        }
    };
    xhr.open(method, path, true);
    xhr.setRequestHeader("OCS-APIRequest", "true");
    if (cred)
        xhr.setRequestHeader("Authorization", "Basic " + cred);
    xhr.setRequestHeader("Content-Type", "application/json");
    if (body)
        xhr.send(JSON.stringify(body));
    else
        xhr.send();
}

function get_boards(boardsModel)
{
    var db = get_db();
    db.transaction(function(tx) {
        var q = "SELECT bid, name, desc, boards.aid, type FROM boards \
                INNER JOIN accounts ON boards.aid = accounts.aid";
        // for now, only `local` type boards are stored in database
        var r = tx.executeSql(q);
        boardsModel.clear();
        for (var i = 0; i < r.rows.length; i++) {
            boardsModel.append({
                bid: r.rows.item(i).bid.toString(),
                name: r.rows.item(i).name,
                desc: r.rows.item(i).desc,
                acc: {
                    aid: r.rows.item(i).aid,
                    type: r.rows.item(i).type
                },
                color: "white",
            });
        }
    });
    db.transaction(function(tx) {
        var q = "SELECT type, url, credentials, aid FROM accounts \
                WHERE type <> 'local' ORDER BY aid";
        var r = tx.executeSql(q);
        var acc;
        for (var i = 0; i < r.rows.length; i++) {
            acc = {
                aid: r.rows.item(i).aid,
                type: r.rows.item(i).type,
                url: r.rows.item(i).url,
                credentials: r.rows.item(i).credentials
            };
            switch (r.rows.item(i).type) {
            case "nextcloud":
                acc.path = "index.php/apps/deck/api/v1.0/";
                send_ajax(
                    function(rt) {
                        rt.forEach(function(cv) {
                            boardsModel.append({
                                bid: cv.id.toString(),
                                name: cv.title,
                                desc: acc.url,
                                acc: acc,
                                color: cv.color,
                            });
                        });
                    },
                    "GET",
                    acc.url + acc.path + "boards",
                    acc.credentials
                );
                break;
            case "trello":
                acc.path = "1/";
                var token = acc.credentials;
                acc.credentials = "?key=429c2f7d14888c5de8260e2d885e6f87";
                acc.credentials += "&token=" + token;
                send_ajax(
                    function(rt) {
                        rt.forEach(function(cv) {
                            if (!cv.closed)
                            boardsModel.append({
                                bid: cv.id.toString(),
                                name: cv.name,
                                desc: cv.desc,
                                acc: acc,
                                color: "blue",
                            });
                        });
                    },
                    "GET",
                    acc.url + acc.path + "members/me/boards" + acc.credentials
                    );
                break;
            }
        }
    });
}

function get_accounts(accountsModel)
{
    var db = get_db();
    db.transaction(function(tx) {
        var q = "SELECT aid, type, url FROM accounts \
                WHERE type <> 'local' ORDER BY aid";
        var r = tx.executeSql(q);
        accountsModel.clear();
        for (var i = 0; i < r.rows.length; i++) {
            accountsModel.append({
                aid: r.rows.item(i).aid.toString(),
                type: r.rows.item(i).type,
                url: r.rows.item(i).url
            });
        }
    });
}

function get_list(lid)
{
    var res = []
    get_db().transaction(function(tx) {
        var q = "SELECT name FROM lists \
                WHERE lid=" + lid + " LIMIT 1";
        var r = tx.executeSql(q);
        res[0] = r.rows.item(0).name;
    });
    return res;
}

function get_card(cid)
{
    var res = []
    get_db().transaction(function(tx) {
        var q = "SELECT name, desc FROM cards \
                WHERE cid=" + cid + " LIMIT 1";
        var r = tx.executeSql(q);
        res[0] = r.rows.item(0).name;
        res[1] = r.rows.item(0).desc;
    });
    return res;
}

function get_notes(cid)
{
    get_db().transaction(function(tx) {
        var q = "SELECT nid, cid, desc, created, author FROM notes \
                WHERE cid=" + cid + " ORDER BY nid";
        var r = tx.executeSql(q);
        cardModel.clear();
        for (var i = 0; i < r.rows.length; i++) {
            cardModel.append({
                                 nid: r.rows.item(i).nid.toString(),
                                 cid: r.rows.item(i).cid.toString(),
                                 desc: r.rows.item(i).desc,
                                 created: r.rows.item(i).created,
                                 author: r.rows.item(i).author});
        }
    });
}

function add_note(cid, text)
{
    get_db().transaction(function(tx) {
        var q = "INSERT INTO notes(cid, desc) \
                VALUES (" + cid + ", '" + text.toString().replace("'", "''") + "')";
        tx.executeSql(q);
    });
}

function delete_note(nid)
{
    get_db().transaction(function(tx) {
        var q = "DELETE FROM notes WHERE nid=" + nid;
        tx.executeSql(q);
    });
}

function change_table(id, params)
{
    get_db().transaction(function(tx) {
        var q = "UPDATE '" + id.tab + "' SET ";
        for (var k in params) {
            q += ((k>0)?", ":"") + params[k][0] + "='";
            q += params[k][1].toString().replace("'", "''") + "'";
        }
        q += " WHERE " + id.idk + "=" + id.idv;
        tx.executeSql(q);
    });
}

function delete_entry(id)
{
    get_db().transaction(function(tx) {
        var q = "DELETE FROM '" + id.tab + "' WHERE " + id.idk + "=" + id.idv;
        tx.executeSql(q);
        if (id.tab === "boards") {
            delete_entry({
                          "tab": "lists",
                          "idk": id.idk,
                          "idv": id.idv});
        }
        if (id.tab === "lists") {
            delete_entry({
                         "tab": "cards",
                         "idk": id.idk,
                         "idv": id.idv});
        }
        if (id.tab === "cards") {
            q = "DELETE FROM notes WHERE nid IN \
                    (SELECT notes.nid FROM notes \
                    LEFT JOIN cards ON notes.cid = cards.cid \
                    WHERE cards.cid IS NULL)";
            tx.executeSql(q);
        }
    });
}

function add_entry(id, params)
{
    get_db().transaction(function(tx) {
        var q = "INSERT INTO " + id.tab + "(";
        for (var k in params) {
            q += ((k>0)?", ":" ") + params[k][0];
        }
        q += ") VALUES (";
        for (k in params) {
            q += ((k>0)?", ":" ") + "'" + params[k][1].toString().replace("'", "''") + "'";
        }
        q += ")";
        tx.executeSql(q);
    });
}
