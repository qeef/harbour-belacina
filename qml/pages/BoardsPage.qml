/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import "../db.js" as Dbjs
import "../board.js" as B

Page {
    id: boardsPage
    allowedOrientations: Orientation.All

    onStatusChanged: {
        if (status == PageStatus.Active) {
            Dbjs.get_boards(boardsModel)
        }
    }

    SilicaListView {
        anchors.fill: parent

        header: Column {
            width: parent.width

            PageHeader {
                title: qsTr("Boards")
            }
        }

        model: ListModel {
            id: boardsModel
        }

        delegate: ListItem {
            id: boardItem
            width: parent.width
            contentHeight: boardName.height +
                           boardDesc.height +
                           Theme.paddingMedium

            menu: ContextMenu {
                MenuItem {
                    enabled: (acc.type === "local") ? true : false
                    text: qsTr("Delete")
                    onClicked: {
                        Remorse.itemAction(boardItem, "Deleting", function() {
                            switch (acc.type) {
                            case "local":
                                Dbjs.delete_entry({
                                    "tab": "boards",
                                    "idk": "bid",
                                    "idv": bid
                                });
                                Dbjs.get_boards(boardsModel);
                                break;
                            }
                        });
                    }
                }
            }

            onClicked: {
                B.cb = new B.Board(bid, name, desc, acc, color);
                pageStack.push(Qt.resolvedUrl("BoardPage.qml"))
            }

            Label {
                id: boardName
                text: name

                color: boardItem.highlighted ?
                           Theme.highlightColor : Theme.primaryColor
                font.pixelSize: Theme.fontSizeLarge
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                height: Text.paintedHeight
                wrapMode: Text.WordWrap
            }
            LinkedLabel {
                id: boardDesc
                text: desc

                color: boardItem.highlighted ?
                           Theme.secondaryHighlightColor :
                           Theme.secondaryColor
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin

                    top: boardName.bottom
                }
                height: Text.paintedHeight
                wrapMode: Text.WordWrap
            }
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Accounts")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("AccountsPage.qml"))
                }
            }
        }
        PushUpMenu {
            MenuItem {
                text: qsTr("Add local board")
                onClicked: {
                    pageStack.push(
                        Qt.resolvedUrl("ChangePage.qml"),
                        {what: "nb"}
                    )
                }
            }
        }
    }
}
