/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import "../db.js" as Dbjs

Page {
    id: accountsPage
    allowedOrientations: Orientation.All

    onStatusChanged: {
        if (status == PageStatus.Active) {
            Dbjs.get_accounts(accountsModel)
        }
    }

    SilicaListView {
        anchors.fill: parent

        header: Column {
            width: parent.width

            PageHeader {
                title: qsTr("Accounts")
            }
        }

        model: ListModel {
            id: accountsModel
        }

        delegate: ListItem {
            id: accountItem
            width: parent.width
            contentHeight: accountType.height +
                           accountURL.height +
                           Theme.paddingMedium

            menu: ContextMenu {
                MenuItem {
                    text: qsTr("Delete")
                    onClicked: {
                        Remorse.itemAction(accountItem, "Deleting", function() {
                            Dbjs.delete_entry({
                                "tab": "accounts",
                                "idk": "aid",
                                "idv": aid
                            });
                            Dbjs.get_accounts(accountsModel);
                        });
                    }
                }
            }

            Label {
                id: accountType
                text: type

                color: accountItem.highlighted ?
                           Theme.highlightColor : Theme.primaryColor
                font.pixelSize: Theme.fontSizeLarge
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                height: Text.paintedHeight
                wrapMode: Text.WordWrap
            }
            LinkedLabel {
                id: accountURL
                text: url

                color: accountItem.highlighted ?
                           Theme.secondaryHighlightColor :
                           Theme.secondaryColor
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin

                    top: accountType.bottom
                }
                height: Text.paintedHeight
                wrapMode: Text.WordWrap
            }
        }

        PushUpMenu {
            MenuItem {
                text: qsTr("Add account")
                onClicked: {
                    pageStack.push(Qt.resolvedUrl("AddAccountPage.qml"))
                }
            }
        }
    }
}
