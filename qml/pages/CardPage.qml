/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import Sailfish.Silica 1.0
import "../board.js" as B
import "../db.js" as Dbjs

Page {
    id: cardPage
    allowedOrientations: Orientation.All

    signal updateSelf()
    onStatusChanged: {
        if (status == PageStatus.Active) {
            updateSelf();
        }
    }

    SilicaListView {
        id: cardPageLV
        anchors.fill: parent

        header: Column {
            width: parent.width

            Connections {
                target: cardPage
                onUpdateSelf: {
                    cardPageName.title = B.cc.getName();
                    cardPageDesc.text = B.cc.getDesc();
                }
            }
            PageHeader {
                id: cardPageName
            }
            LinkedLabel {
                id: cardPageDesc

                font.pixelSize: Theme.fontSizeSmall
                color: Theme.secondaryColor
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                wrapMode: Text.WordWrap
            }
            Separator {
                color: Theme.secondaryColor
                horizontalAlignment: Qt.AlignHCenter

                anchors {
                    left: parent.left
                    right: parent.right
                }
            }
        }

        model: ListModel {
            id: cardModel
            Component.onCompleted: {
                B.cc.getNotes(cardModel);
            }
        }

        delegate: ListItem {
            id: noteItem
            width: parent.width
            contentHeight: noteHeader.height +
                           noteDesc.height +
                           Theme.paddingMedium

            menu: ContextMenu {
                MenuItem {
                    enabled: (B.cc.acc.type === "local") ? true : false
                    text: qsTr("Delete")
                    onClicked: {
                        Remorse.itemAction(noteItem, "Deleting", function() {
                            Dbjs.delete_note(cardModel.get(index).nid);
                            B.cc.getNotes(cardModel);
                        }, 2000);
                    }
                }
            }

            Label {
                id: noteHeader
                text: qsTr("On %L1, %2 wrote:")
                        .arg((new Date(created+"Z")).toLocaleString(
                                Qt.locale(),
                                Locale.ShortFormat))
                        .arg(author);
                font.pixelSize: Theme.fontSizeTiny
                color: Theme.secondaryColor
                anchors {
                    left: parent.left
                    leftMargin: Theme.horizontalPageMargin
                }
                height: Text.paintedHeight
            }
            LinkedLabel {
                id: noteDesc
                plainText: desc

                color: noteItem.highlighted ?
                           Theme.highlightColor : Theme.primaryColor
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin

                    top: noteHeader.bottom
                }
                height: Text.paintedHeight
                wrapMode: Text.WordWrap
            }
        }

        footer: TextArea {
                id: addNote
                width: parent.width
                placeholderText: qsTr("Put your note here")
                visible: (B.cc.acc.type !== "nextcloud") ? true : false

                EnterKey.enabled: text.length > 0
                EnterKey.iconSource: "image://theme/icon-m-enter-accept"
                EnterKey.onClicked: {
                    focus = false
                    B.cc.addNote(addNote.text.trim());
                    addNote.text = "";
                    B.cc.getNotes(cardModel);
                }
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Change card")
                onClicked: {
                    pageStack.push(
                        Qt.resolvedUrl("ChangePage.qml"),
                        {
                            what: "cc",
                            name: B.cc.getName(),
                            desc: B.cc.getDesc()
                        }
                    )
                }
            }
        }
    }
}
