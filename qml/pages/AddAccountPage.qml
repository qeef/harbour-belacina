/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import "../db.js" as Dbjs

Dialog {
    id: addAccountPage
    allowedOrientations: Orientation.All

    onAccepted: {
        if (accountType.value === "nextcloud")
            Dbjs.add_entry({
                               "tab": "accounts"
                           }, [
                               ["type", accountType.value],
                               ["url", (accountURL.text.slice(-1) !== "/")?
                                    accountURL.text+"/":
                                    accountURL.text],
                               ["credentials", Qt.btoa(
                                    accountUsername.text + ":" +
                                    accountPassword.text)]
                           ]);
        else if (accountType.value === "trello")
            Dbjs.add_entry({
                               "tab": "accounts"
                           }, [
                               ["type", accountType.value],
                               ["url", (accountURL.text.slice(-1) !== "/")?
                                    accountURL.text+"/":
                                    accountURL.text],
                               ["credentials", accountToken.text.trim()]
                           ]);
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: accountHeader.height +
                       accountType.height +
                       accountURL.height +
                       accountUsername.height +
                       accountPassword.height

        DialogHeader {
            id: accountHeader
            title: qsTr("Add account")
        }
        ComboBox {
            id: accountType
            anchors.top: accountHeader.bottom
            width: parent.width
            label: qsTr("Account type")
            menu: ContextMenu {
                MenuItem {
                    text: "nextcloud"
                    onClicked: {
                        accountTokenRequest.visible = false
                        accountToken.visible = false
                        accountURL.visible = true
                        accountURL.text = "https://"
                        accountUsername.visible = true
                        accountPassword.visible = true
                        accountUsername.label = qsTr("Username. Don't use `:` in username.")
                    }
                }
                MenuItem {
                    text: "trello"
                    onClicked: {
                        accountUsername.visible = false
                        accountPassword.visible = false
                        accountURL.visible = false
                        accountURL.text = "https://api.trello.com/"
                        accountTokenRequest.visible = true
                        accountToken.visible = true

                    }
                }
            }
        }
        TextArea {
            id: accountURL
            anchors.top: accountType.bottom
            width: parent.width
            text: "https://"
            label: qsTr("URL")
            placeholderText: qsTr("URL")
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhUrlCharactersOnly
        }
        TextArea {
            id: accountUsername
            anchors.top: accountURL.bottom
            width: parent.width
            text: ""
            label: qsTr("Username. Don't use `:` in username.")
            placeholderText: qsTr("Username")
            inputMethodHints: Qt.ImhNoAutoUppercase
            visible: true
        }
        PasswordField {
            id: accountPassword
            anchors.top: accountUsername.bottom
            width: parent.width
            text: ""
            label: qsTr("Password")
            placeholderText: qsTr("Password")
            visible: true
        }
        Button {
            id: accountTokenRequest
            anchors.top: accountType.bottom
            width: parent.width
            text: "Authorize on Twitter"
            onClicked: Qt.openUrlExternally("https://trello.com/1/authorize?expiration=never&name=Belacina&scope=read,write&response_type=token&key=429c2f7d14888c5de8260e2d885e6f87")
            visible: false
        }
        TextArea {
            id: accountToken
            anchors.top: accountTokenRequest.bottom
            width: parent.width
            text: ""
            label: qsTr("Copy token here")
            placeholderText: qsTr("Copy token here")
            inputMethodHints: Qt.ImhNoAutoUppercase
            visible: false
        }
    }
}
