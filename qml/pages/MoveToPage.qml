/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import "../board.js" as B

Dialog {
    id: moveToPage
    allowedOrientations: Orientation.All

    Loader {
        Component.onCompleted: {
            B.cb.getLists(boardModel);
        }
    }

    SilicaListView {
        anchors.fill: parent

        header: Column {
            width: parent.width

            DialogHeader {
                title: qsTr("Move to")
            }
        }

        model: ListModel {
            id: boardModel
        }

        delegate: ListItem {
            id: listItem
            width: parent.width
            contentHeight: listName.height + Theme.paddingMedium

            onClicked: {
                B.cc.lid = lid;
                B.cc.updateSelf();
                pageStack.pop();
            }

            Label {
                id: listName
                text: name

                color: listItem.highlighted ?
                           Theme.highlightColor : Theme.primaryColor
                font.pixelSize: Theme.fontSizeLarge
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                height: Text.paintedHeight
                wrapMode: Text.WordWrap
            }
        }
    }
}
