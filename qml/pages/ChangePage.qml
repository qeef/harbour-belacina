/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import "../board.js" as B
import "../db.js" as Dbjs

Dialog {
    id: changePage
    allowedOrientations: Orientation.All

    property string name
    property string desc
    property string what

    onAccepted: {
        switch (what) {
        case "cc":
            B.cc.name = changeName.text;
            B.cc.desc = changeDesc.text;
            B.cc.updateSelf();
            break;
        case "cl":
            B.cl.name = changeName.text;
            B.cl.updateSelf();
            break;
        case "cb":
            B.cb.name = changeName.text;
            B.cb.desc = changeDesc.text;
            B.cb.updateSelf();
            break;
        case "nc":
            B.cl.addCard(changeName.text, changeDesc.text);
            break;
        case "nl":
            B.cb.addList(changeName.text);
            break;
        case "nb":
            Dbjs.add_entry({
                               "tab": "boards"
                           }, [
                               ["name", changeName.text],
                               ["desc", changeDesc.text]]);
            break;
        default:
            console.log(sqTr("Nothing"));
            break;
        }
    }

    Loader {
        Component.onCompleted: {
            changeDesc.visible = true;
            switch (what) {
            case "cc":
                changeHeader.title = qsTr("Change card");
                break;
            case "cl":
                changeHeader.title = qsTr("Change list");
                changeDesc.visible = false;
                break;
            case "cb":
                changeHeader.title = qsTr("Change board");
                if (B.cb.acc.type === "nextcloud")
                    changeDesc.visible = false;
                break;
            case "nc":
                changeHeader.title = qsTr("New card");
                break;
            case "nl":
                changeHeader.title = qsTr("New list");
                changeDesc.visible = false;
                break;
            case "nb":
                changeHeader.title = qsTr("New board");
                break;
            default:
                changeHeader.title = qsTr("Nothing");
                break;
            }
        }
    }

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: changeHeader.height +
                       changeName.height +
                       changeDesc.height

        DialogHeader {
            id: changeHeader
            title: qsTr("Nothing")
        }
        TextArea {
            id: changeName
            anchors.top: changeHeader.bottom
            width: parent.width
            text: name ? name : ""
            label: qsTr("Name")
            placeholderText: qsTr("Name")
        }
        TextArea {
            id: changeDesc
            anchors.top: changeName.bottom
            width: parent.width
            text: desc ? desc : ""
            label: qsTr("Description")
            placeholderText: qsTr("Description")
        }
    }
}
