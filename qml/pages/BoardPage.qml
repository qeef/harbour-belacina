/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import Sailfish.Silica 1.0
import "../board.js" as B

Page {
    id: boardPage
    allowedOrientations: Orientation.All

    signal updateSelf()
    onStatusChanged: {
        if (status == PageStatus.Active) {
            updateSelf();
            B.cl = null;
            B.cb.getLists(boardModel);
        }
    }

    SilicaListView {
        anchors.fill: parent

        header: Column {
            width: parent.width
            Connections {
                target: boardPage
                onUpdateSelf: {
                    boardPageName.title = B.cb.getName();
                    boardPageDesc.text = B.cb.getDesc();
                }
            }

            PageHeader {
                id: boardPageName
            }
            LinkedLabel {
                id: boardPageDesc

                font.pixelSize: Theme.fontSizeSmall
                color: Theme.secondaryColor
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                wrapMode: Text.WordWrap
            }
            Separator {
                color: Theme.secondaryColor
                horizontalAlignment: Qt.AlignHCenter

                anchors {
                    left: parent.left
                    right: parent.right
                }
            }
        }

        model: ListModel {
            id: boardModel
        }

        delegate: ListItem {
            id: listItem
            width: parent.width
            contentHeight: listName.height + Theme.paddingMedium

            menu: ContextMenu {
                MenuItem {
                    text: qsTr("Delete")
                    onClicked: {
                        Remorse.itemAction(listItem, "Deleting", function() {
                            var tmplist = new B.List(lid, bid);
                            tmplist.deleteSelf();
                            B.cb.getLists(boardModel);
                        });
                    }
                }
            }

            onClicked: {
                B.cl = new B.List(lid, bid, name, cards, order)
                pageStack.push(Qt.resolvedUrl("ListPage.qml"))
            }

            Label {
                id: listName
                text: name

                color: listItem.highlighted ?
                           Theme.highlightColor : Theme.primaryColor
                font.pixelSize: Theme.fontSizeLarge
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                height: Text.paintedHeight
                wrapMode: Text.WordWrap
            }
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Change board")
                onClicked: {
                    pageStack.push(
                        Qt.resolvedUrl("ChangePage.qml"),
                        {
                            what: "cb",
                            name: B.cb.getName(),
                            desc: B.cb.getDesc(),
                        }
                    )
                }
            }
        }
        PushUpMenu {
            MenuItem {
                text: qsTr("Add list")
                onClicked: {
                    pageStack.push(
                        Qt.resolvedUrl("ChangePage.qml"),
                        {what: "nl"}
                    )
                }
            }
        }
    }
}
