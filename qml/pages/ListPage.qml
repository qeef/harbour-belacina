/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import Sailfish.Silica 1.0
import "../db.js" as Dbjs
import "../board.js" as B

Page {
    id: listPage
    allowedOrientations: Orientation.All

    signal updateSelf()
    onStatusChanged: {
        if (status == PageStatus.Active) {
            updateSelf();
            B.cc = null;
            B.cl.getCards(listModel);
        }
    }

    SilicaListView {
        anchors.fill: parent

        header: PageHeader {
            id: listPageName
            Connections {
                target: listPage
                onUpdateSelf: {
                    listPageName.title = B.cl.getName();
                }
            }
        }

        model: ListModel {
            id: listModel
        }

        delegate: ListItem {
            id: cardItem
            width: parent.width
            contentHeight: cardName.height +
                           cardDesc.height +
                           Theme.paddingMedium

            menu: ContextMenu {
                MenuItem {
                    text: qsTr("Move to")
                    onClicked: {
                        B.cc = new B.Card(
                            cid,
                            lid,
                            bid,
                            name,
                            desc,
                            type,
                            owner
                        );
                        pageStack.push(Qt.resolvedUrl("MoveToPage.qml"))
                    }
                }
                MenuItem {
                    text: qsTr("Delete")
                    onClicked: {
                        Remorse.itemAction(cardItem, "Deleting", function() {
                            var tmpcard = new B.Card(cid, lid, bid, name, desc);
                            tmpcard.deleteSelf();
                            for (var i = 0; i < B.cl.cards.length; i++) {
                                if (B.cl.cards[i]
                                        && B.cl.cards[i].id === tmpcard.cid) {
                                    delete B.cl.cards[i];
                                    break;
                                }
                            }
                            B.cl.getCards(listModel);
                        });
                    }
                }
            }

            onClicked: {
                B.cc = new B.Card(cid, lid, bid, name, desc, type, owner);
                pageStack.push(Qt.resolvedUrl("CardPage.qml"));
            }

            Label {
                id: cardName
                text: name

                color: cardItem.highlighted ?
                           Theme.highlightColor : Theme.primaryColor
                font.pixelSize: Theme.fontSizeLarge
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                }
                height: Text.paintedHeight
                wrapMode: Text.WordWrap
            }
            LinkedLabel {
                id: cardDesc
                text: desc

                color: cardItem.highlighted ?
                           Theme.secondaryHighlightColor :
                           Theme.secondaryColor
                anchors {
                    left: parent.left
                    right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin

                    top: cardName.bottom
                }
                height: Text.paintedHeight
                wrapMode: Text.WordWrap
            }
        }

        PullDownMenu {
            MenuItem {
                text: qsTr("Sort by name descending")
                visible: (B.cl.acc.type === "local") ? true : false
                onClicked: {
                    switch (B.cl.acc.type) {
                    case "local":
                        Dbjs.change_table({
                                          "tab": "lists",
                                          "idk": "lid",
                                          "idv": B.cl.lid
                                          }, [
                                              ["sort", "name DESC"]
                                          ]);
                        B.cl.getCards(listModel)
                        break;
                    }
                }
            }
            MenuItem {
                text: qsTr("Sort by name")
                visible: (B.cl.acc.type === "local") ? true : false
                onClicked: {
                    switch (B.cl.acc.type) {
                    case "local":
                        Dbjs.change_table({
                                          "tab": "lists",
                                          "idk": "lid",
                                          "idv": B.cl.lid
                                          }, [
                                              ["sort", "name"]
                                          ]);
                        B.cl.getCards(listModel)
                        break;
                    }
                }
            }
            MenuItem {
                text: qsTr("Sort by time of creation descending")
                visible: (B.cl.acc.type === "local") ? true : false
                onClicked: {
                    switch (B.cl.acc.type) {
                    case "local":
                        Dbjs.change_table({
                                          "tab": "lists",
                                          "idk": "lid",
                                          "idv": B.cl.lid
                                          }, [
                                              ["sort", "created DESC"]
                                          ]);
                        B.cl.getCards(listModel)
                        break;
                    }
                }
            }
            MenuItem {
                text: qsTr("Sort by time of creation")
                visible: (B.cl.acc.type === "local") ? true : false
                onClicked: {
                    switch (B.cl.acc.type) {
                    case "local":
                        Dbjs.change_table({
                                          "tab": "lists",
                                          "idk": "lid",
                                          "idv": B.cl.lid
                                          }, [
                                              ["sort", "created"]
                                          ]);
                        B.cl.getCards(listModel)
                        break;
                    }
                }
            }
            MenuItem {
                text: qsTr("Change list")
                onClicked: {
                    pageStack.push(
                        Qt.resolvedUrl("ChangePage.qml"),
                        {
                            what: "cl",
                            name: B.cl.getName(),
                        }
                    )
                }
            }
        }
        PushUpMenu {
            MenuItem {
                text: qsTr("Add card")
                onClicked: {
                    pageStack.push(
                        Qt.resolvedUrl("ChangePage.qml"),
                        {what: "nc"}
                    )
                }
            }
        }
    }
}
