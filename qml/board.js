/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
.pragma library
.import "db.js" as Dbjs

var cb; // current board
var cl; // current list
var cc; // current card

function Board(bid, name, desc, acc, color) {
    this.bid = bid;
    this.name = name;
    this.desc = desc;
    this.acc = acc;
    this.color = color;
}

Board.prototype = {
    constructor: Board,
    logall: function() {
        console.log(JSON.stringify(this));
    },
    getName: function() {
        return this.name;
    },
    getDesc: function() {
        return this.desc;
    },
    getLists: function(boardModel) {
        boardModel.clear();
        var thisbid = this.bid;

        switch (this.acc.type) {
        case "local":
            Dbjs.get_db().transaction(function(tx) {
                var q = "SELECT lid, bid, name FROM lists \
                        WHERE bid=" + thisbid + " ORDER BY lid";
                var r = tx.executeSql(q);
                for (var i = 0; i < r.rows.length; i++) {
                    boardModel.append({
                        lid: r.rows.item(i).lid,
                        bid: r.rows.item(i).bid,
                        name: r.rows.item(i).name,
                        cards: JSON.stringify([]),
                        order: 999
                    });
                }
            });
            break;
        case "nextcloud":
            Dbjs.send_ajax(
                function(rt) {
                    rt.forEach(function(cv) {
                        boardModel.append({
                            lid: cv.id,
                            bid: cv.boardId,
                            name: cv.title,
                            cards: JSON.stringify((cv.cards) ? cv.cards : "[]"),
                            order: JSON.stringify((cv.order) ? cv.order : "999"),
                        });
                    });
                },
                "GET",
                this.acc.url + this.acc.path + "boards/" + this.bid + "/stacks",
                this.acc.credentials
            );
            break;
        case "trello":
            Dbjs.send_ajax(
                function(rt) {
                    rt.forEach(function(cv) {
                        if (!cv.closed)
                        boardModel.append({
                            lid: cv.id,
                            bid: cv.idBoard,
                            name: cv.name,
                            cards: "[]",
                            order: JSON.stringify((cv.pos) ? cv.pos : "999"),
                        });
                    });
                },
                "GET",
                this.acc.url + this.acc.path + "boards/" + this.bid + "/lists" +
                this.acc.credentials
            );
            break;
        }
    },
    addList: function(name) {
        var thisbid;
        var thisacc;
        var addpath;
        switch (this.acc.type) {
        case "local":
            Dbjs.add_entry(
                {"tab": "lists"},
                [
                    ["bid", this.bid],
                    ["name", name]
                ]
            );
            break;
        case "nextcloud":
            var tmpcard = null;
            thisbid = this.bid;
            thisacc = this.acc;
            addpath = "boards/" + this.bid + "/";
            addpath += "stacks";
            Dbjs.send_ajax(
                function(rt) {},
                "POST",
                this.acc.url + this.acc.path + addpath,
                this.acc.credentials,
                {title: name, order: 999}
            );
            break;
        case "trello":
            thisbid = this.bid;
            thisacc = this.acc;
            addpath = "lists";
            Dbjs.send_ajax(
                function(rt) {},
                "POST",
                this.acc.url + this.acc.path + addpath +
                this.acc.credentials +
                "&name=" + name + "&idBoard=" + thisbid
            );
            break;
        }
    },
    updateSelf: function() {
        var thisacc;
        var updatepath;
        var body;
        switch (this.acc.type) {
        case "local":
            Dbjs.change_table(
                {"tab": "boards", "idk": "bid", "idv": this.bid},
                [
                    ["name", this.name],
                    ["desc", this.desc],
                ]
            );
            break;
        case "nextcloud":
            thisacc = this.acc;
            updatepath = "boards/" + this.bid;
            body = {
                title: this.name,
                color: this.color,
            }
            Dbjs.send_ajax(
                function(rt) {},
                "PUT",
                thisacc.url + thisacc.path + updatepath,
                thisacc.credentials,
                body
            );
            break;
        case "trello":
            thisacc = this.acc;
            updatepath = "boards/" + this.bid;
            body = {
                name: this.name,
                desc: this.desc
            };
            Dbjs.send_ajax(
                function(rt) {},
                "PUT",
                thisacc.url + thisacc.path + updatepath +
                thisacc.credentials +
                "&name=" + body.name + "&desc=" + body.desc
            );
            break;
        }
    }
}

function List(lid, bid, name, cards, order) {
    this.lid = lid;
    this.bid = bid;
    this.name = name;
    this.acc = cb.acc;
    this.cards = (cards) ? JSON.parse(cards) : [];
    this.lm = null;
    this.order = order;
}

List.prototype = {
    constructor: List,
    logall: function() {
        console.log(JSON.stringify(this));
    },
    getName: function() {
        return this.name;
    },
    getCards: function(listModel) {
        var pthis;
        var getpath;
        if (listModel)
            this.lm = listModel;
        else
            listModel = this.lm;
        listModel.clear();
        var thislid = this.lid;
        switch (this.acc.type) {
        case "local":
            var sort_by = "";
            Dbjs.get_db().transaction(function(tx) {
                var q = "SELECT sort FROM lists \
                        WHERE lid=" + thislid;
                var r = tx.executeSql(q);
                sort_by = r.rows.item(0).sort;
            });
            Dbjs.get_db().transaction(function(tx) {
                var q = "SELECT cid, lid, bid, name, desc FROM cards \
                        WHERE lid=" + thislid + " ORDER BY " + sort_by;
                var r = tx.executeSql(q);
                for (var i = 0; i < r.rows.length; i++) {
                    listModel.append({
                        cid: r.rows.item(i).cid,
                        lid: r.rows.item(i).lid,
                        bid: r.rows.item(i).bid,
                        name: r.rows.item(i).name,
                        type: "",
                        owner: "",
                        desc: r.rows.item(i).desc
                    });
                }
            });
            break;
        case "nextcloud":
            pthis = this;
            getpath = "boards/" + this.bid + "/";
            getpath += "stacks/" + this.lid;
            Dbjs.send_ajax(
                function(rt) {
                    if (rt.cards)
                        pthis.cards = rt.cards;
                    else
                        pthis.cards = [];
                    pthis.cards.forEach(function(cv) {
                        listModel.append({
                            cid: cv.id,
                            lid: cv.stackId,
                            bid: cb.bid,
                            name: cv.title,
                            type: cv.type,
                            owner: cv.owner,
                            desc: (cv.description) ? cv.description : ""
                        });
                    });
                },
                "GET",
                this.acc.url + this.acc.path + getpath,
                this.acc.credentials
            );
            break;
        case "trello":
            pthis = this;
            Dbjs.send_ajax(
                function(rt) {
                    pthis.cards = rt;
                    pthis.cards.forEach(function(cv) {
                        if (!cv.closed)
                        listModel.append({
                            cid: cv.id,
                            lid: cv.idList,
                            bid: cv.idBoard,
                            name: cv.name,
                            type: "",
                            owner: "",
                            desc: (cv.desc) ? cv.desc : ""
                        });
                    });
                },
                "GET",
                this.acc.url + this.acc.path + "lists/" + this.lid + "/cards" +
                this.acc.credentials
            );
            break;
        }
    },
    addCard: function(name, desc) {
        switch (this.acc.type) {
        case "local":
            Dbjs.add_entry(
                {"tab": "cards"},
                [
                    ["lid", this.lid],
                    ["bid", this.bid],
                    ["name", name],
                    ["desc", desc]
                ]
            );
            break;
        case "nextcloud":
            var tmpcard = null;
            var thisbid = this.bid;
            var thislid = this.lid;
            var thisacc = this.acc;
            var addpath = "boards/" + this.bid + "/";
            addpath += "stacks/" + this.lid + "/";
            addpath += "cards";
            Dbjs.send_ajax(
                function(rt) {
                    var updatepath = "boards/" + thisbid + "/";
                    updatepath += "stacks/" + thislid + "/";
                    updatepath += "cards/" + rt.id;
                    var body = {
                        title: name,
                        type: rt.type,
                        owner: rt.owner,
                        description: desc
                    }
                    Dbjs.send_ajax(
                        function(rt2) {},
                        "PUT",
                        thisacc.url + thisacc.path + updatepath,
                        thisacc.credentials,
                        body
                    );
                },
                "POST",
                this.acc.url + this.acc.path + addpath,
                this.acc.credentials,
                {title: name, type: "plain", order: 999}
            );
            break;
        case "trello":
            Dbjs.send_ajax(
                function(rt) {},
                "POST",
                this.acc.url + this.acc.path + "cards" +
                this.acc.credentials +
                "&idList=" + this.lid + "&keepFromSource=all" +
                "&name=" + name + "&desc=" + desc
            );
            break;
        }
    },
    deleteSelf: function() {
        switch (this.acc.type) {
        case "local":
            Dbjs.delete_entry({"tab": "lists", "idk": "lid", "idv": this.lid});
            break;
        case "nextcloud":
            var delpath = "boards/" + this.bid + "/";
            delpath += "stacks/" + this.lid;
            Dbjs.send_ajax(
                function(rt) {},
                "DELETE",
                this.acc.url + this.acc.path + delpath,
                this.acc.credentials
            );
            break;
        case "trello":
            Dbjs.send_ajax(
                function(rt) {},
                "PUT",
                this.acc.url + this.acc.path + "lists/" + this.lid +
                this.acc.credentials +
                "&closed=true"
            );
            break;
        }
    },
    updateSelf: function() {
        switch (this.acc.type) {
        case "local":
            Dbjs.change_table(
                {"tab": "lists", "idk": "lid", "idv": this.lid},
                [
                    ["name", this.name],
                ]
            );
            break;
        case "nextcloud":
            var thisacc = this.acc;
            var updatepath = "boards/" + this.bid + "/";
            updatepath += "stacks/" + this.lid;
            var body = {
                title: this.name,
                order: this.order
            }
            Dbjs.send_ajax(
                function(rt) {},
                "PUT",
                thisacc.url + thisacc.path + updatepath,
                thisacc.credentials,
                body
            );
            break;
        case "trello":
            Dbjs.send_ajax(
                function(rt) {},
                "PUT",
                this.acc.url + this.acc.path + "lists/" + this.lid +
                this.acc.credentials +
                "&name=" + this.name
            );
            break;
        }
    }
}

function Card(cid, lid, bid, name, desc, type, owner) {
    this.cid = cid;
    this.lid = lid;
    this.bid = bid;
    this.name = name;
    this.desc = desc;
    this.acc = cl.acc;
    this.type = type;
    this.owner = owner;
}

Card.prototype = {
    constructor: Card,
    logall: function() {
        console.log(JSON.stringify(this));
    },
    getName: function() {
        return this.name;
    },
    getDesc: function() {
        return this.desc;
    },
    getNotes: function(cardModel) {
        cardModel.clear();
        var thiscid = this.cid;
        switch (this.acc.type) {
        case "local":
            Dbjs.get_db().transaction(function(tx) {
                var q = "SELECT nid, cid, desc, created, author FROM notes \
                        WHERE cid=" + thiscid + " ORDER BY nid";
                var r = tx.executeSql(q);
                for (var i = 0; i < r.rows.length; i++) {
                    cardModel.append({
                                         nid: r.rows.item(i).nid,
                                         cid: r.rows.item(i).cid,
                                         desc: r.rows.item(i).desc,
                                         created: r.rows.item(i).created,
                                         author: r.rows.item(i).author});
                }
            });
            break;
        case "trello":
            Dbjs.send_ajax(
                function(rt) {
                    rt.forEach(function(cv) {
                        if (cv.data && cv.data.text && cv.data.text !== "")
                        cardModel.append({
                            nid: cv.id,
                            cid: cv.data.card.id,
                            desc: cv.data.text,
                            created: cv.date,
                            author: cv.memberCreator.fullName
                        });
                    });
                },
                "GET",
                this.acc.url + this.acc.path + "cards/" + this.cid + "/actions"+
                this.acc.credentials
            );
            break;
        }
    },
    addNote: function(desc) {
        switch (this.acc.type) {
        case "local":
            Dbjs.add_note(this.cid, desc);
            break;
        case "trello":
            Dbjs.send_ajax(
                function(rt) {},
                "POST",
                this.acc.url + this.acc.path + "cards/" + this.cid +
                "/actions/comments" +
                this.acc.credentials +
                "&text=" + desc
            );
            break;
        }
    },
    deleteSelf: function() {
        switch (this.acc.type) {
        case "local":
            Dbjs.delete_entry({"tab": "cards", "idk": "cid", "idv": this.cid});
            break;
        case "nextcloud":
            var delpath = "boards/" + this.bid + "/";
            delpath += "stacks/" + this.lid + "/";
            delpath += "cards/" + this.cid;
            Dbjs.send_ajax(
                function(rt) {},
                "DELETE",
                this.acc.url + this.acc.path + delpath,
                this.acc.credentials
            );
            break;
        case "trello":
            Dbjs.send_ajax(
                function(rt) {},
                "PUT",
                this.acc.url + this.acc.path + "cards/" + this.cid +
                this.acc.credentials +
                "&closed=true"
            );
            break;
        }
    },
    updateSelf: function() {
        switch (this.acc.type) {
        case "local":
            Dbjs.change_table(
                {"tab": "cards", "idk": "cid", "idv": this.cid},
                [
                    ["name", this.name],
                    ["desc", this.desc],
                    ["lid", this.lid]
                ]
            );
            break;
        case "nextcloud":
            var thisacc = this.acc;
            var updatepath = "boards/" + this.bid + "/";
            updatepath += "stacks/" + this.lid + "/";
            updatepath += "cards/" + this.cid;
            var body = {
                title: this.name,
                type: this.type,
                owner: this.owner,
                description: this.desc,
                stackId: this.lid
            }
            Dbjs.send_ajax(
                function(rt) {},
                "PUT",
                thisacc.url + thisacc.path + updatepath,
                thisacc.credentials,
                body
            );
            break;
        case "trello":
            Dbjs.send_ajax(
                function(rt) {},
                "PUT",
                this.acc.url + this.acc.path + "cards/" + this.cid +
                this.acc.credentials +
                "&name=" + this.name +
                "&desc=" + this.desc +
                "&idList=" + this.lid
            );
            break;
        }
    }
}
