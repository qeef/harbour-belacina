/*
    This file is part of Belacina.

    Belacina is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Belacina is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Belacina.  If not, see <https://www.gnu.org/licenses/>.
*/
import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import Sailfish.Silica 1.0
import "pages"
import "db.js" as Dbjs

ApplicationWindow
{
    initialPage: Component { BoardsPage { } }
    cover: Qt.resolvedUrl("cover/CoverPage.qml")
    allowedOrientations: defaultAllowedOrientations

    Loader {
        Component.onCompleted: {
            var boards_table = "CREATE TABLE boards(bid INTEGER PRIMARY KEY, name TEXT, desc TEXT)";
            var lists_table = "CREATE TABLE lists(lid INTEGER PRIMARY KEY, bid INTEGER, name TEXT)";
            var cards_table = "CREATE TABLE cards(cid INTEGER PRIMARY KEY, lid INTEGER, bid INTEGER, name TEXT, desc TEXT)";
            var notes_table = "CREATE TABLE notes(nid INTEGER PRIMARY KEY, cid INTEGER, desc TEXT)";
            var db = LocalStorage.openDatabaseSync(
                        "belacina.db",
                        "",
                        "Storage for boards, lists, cards, and notes.",
                        10000000,
                    function(db) {
                        db.transaction(function (tx) {
                            tx.executeSql(boards_table);
                        });
                        db.transaction(function (tx) {
                            tx.executeSql(lists_table);
                        });
                        db.transaction(function (tx) {
                            tx.executeSql(cards_table);
                        });
                        db.transaction(function (tx) {
                            tx.executeSql(notes_table);
                        });
                        db.changeVersion("", "1");
                    })
            if (db.version === "")
                db = Dbjs.get_db()
            if (db.version === "1") {
                db.changeVersion("1", "2", function(tx) {
                    tx.executeSql("ALTER TABLE lists ADD sort TEXT NOT NULL default 'cid'");

                    tx.executeSql("ALTER TABLE cards RENAME TO cards_v1");
                    tx.executeSql("CREATE TABLE cards(cid INTEGER PRIMARY KEY, lid INTEGER, bid INTEGER, name TEXT, desc TEXT, created TEXT NOT NULL default current_timestamp)");
                    tx.executeSql("INSERT INTO cards(cid, lid, bid, name, desc) SELECT cid, lid, bid, name, desc FROM cards_v1");

                    tx.executeSql("ALTER TABLE notes RENAME TO notes_v1");
                    tx.executeSql("CREATE TABLE notes(nid INTEGER PRIMARY KEY, cid INTEGER, desc TEXT, created TEXT NOT NULL default current_timestamp, author TEXT NOT NULL default 'Myself')");
                    tx.executeSql("INSERT INTO notes(nid, cid, desc) SELECT nid, cid, desc FROM notes_v1");

                    tx.executeSql("CREATE TABLE IF NOT EXISTS accounts (aid INTEGER PRIMARY KEY, type TEXT, url TEXT, credentials TEXT)");
                })
            }
            if (db.version === "1")
                db = Dbjs.get_db()
            if (db.version === "2") {
                db.changeVersion("2", "3", function(tx) {
                    tx.executeSql("INSERT INTO accounts(type) VALUES('local')");
                    var r = tx.executeSql("SELECT MAX(aid) as aid FROM accounts WHERE type='local'");
                    var aid = r.rows.item(0).aid;
                    tx.executeSql("ALTER TABLE boards ADD aid INT NOT NULL default " + aid);
                    tx.executeSql("ALTER TABLE lists ADD aid INT NOT NULL default " + aid);
                    tx.executeSql("ALTER TABLE cards ADD aid INT NOT NULL default " + aid);
                    tx.executeSql("ALTER TABLE notes ADD aid INT NOT NULL default " + aid);
                })
            }
            if (db.version === "2")
                db = Dbjs.get_db()
        }
    }
}
