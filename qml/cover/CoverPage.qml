import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {
    id: bgCover

    Image {
        id: imgCover
        source: "/usr/share/icons/hicolor/172x172/apps/harbour-belacina.png"
        fillMode: Image.Stretch
        anchors {
            left: parent.left
            right: parent.right
        }
        height: width
    }

    Label {
        text: "Belacina"
        anchors {
            left: parent.left
            right: parent.right
            leftMargin: Theme.horizontalPageMargin
            rightMargin: Theme.horizontalPageMargin

            top: imgCover.bottom
        }
        height: bgCover.height - imgCover.height
        horizontalAlignment: Qt.AlignHCenter
        verticalAlignment: Qt.AlignVCenter
    }
}
