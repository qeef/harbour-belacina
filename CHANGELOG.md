# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog][] and this project adheres to
[Semantic Versioning][].

[Keep a Changelog]: http://keepachangelog.com/
[Semantic Versioning]: http://semver.org/

## [Unreleased][]

## [0.3.2][] - 2019-05-13
### Fixed
- Fix database adding/changing functions (#8).

## [0.3.1][] - 2019-04-29
### Fixed
- Add apostrophe to database (#6).

## [0.3.0][] - 2019-04-12
### Added
- Accounts page for managing (non-local) accounts.
- Basic Trello functionality:
    - Add/delete account.
    - Change board (adding/deleting boards not yet implemented).
    - Add/change/delete list.
    - Add/change/delete cards.
    - Move cards between lists.
    - Add comments to card.

## [0.2.0][] - 2019-03-01
### Added
- Option to sort (local) cards on list.
- Basic NextCloud functionality:
    - Add account (account manager not yet implemented).
    - Change board (adding/deleting boards not yet implemented).
    - Add/change/delete list.
    - Add/change/delete cards.
    - Move cards between lists.

### Fixed
- Author and creation time of notes.

## 0.1.0 - 2019-02-02
### Added
- Changelog, license, readme.
- Basic navigation in app.
- Add/change/delete boards, lists, cards, and notes.

[Unreleased]: https://gitlab.com/qeef/harbour-belacina/compare/v0.3.2...master
[0.3.2]: https://gitlab.com/qeef/harbour-belacina/compare/v0.3.1...v0.3.2
[0.3.1]: https://gitlab.com/qeef/harbour-belacina/compare/v0.3.0...v0.3.1
[0.3.0]: https://gitlab.com/qeef/harbour-belacina/compare/v0.2.0...v0.3.0
[0.2.0]: https://gitlab.com/qeef/harbour-belacina/compare/v0.1.0...v0.2.0
